<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/longpn/week2/connecting/connectDB.php';
if (!isset($_SESSION["hoTen"])) {
    header("location: ./account/login.php");
}
$ava = $_SESSION["avatar"];
if (isset($_SESSION["avatar_update"])) {
    $ava = $_SESSION["avatar_update"];
}
$contents = "";
if (isset($_GET['admin'])) {

    switch ($_GET['admin']) {
        case 'dashboard':
            include_once './component/dashboard.php';
            break;
        case 'logs':
            include_once './component/logs.php';
            break;
        case 'setting':
            include_once './component/setting.php';
            break;
        case 'update':
            include_once './component/update.php';
            break;
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once './connecting/eleHead.php'; ?>
</head>

<body>
    <div class="admin">

        <!-- sidebar -->
        <div class="admin_sidebar">

            <div><a class="admin_index" href="#"><i class="fas fa-laptop-code"></i> Device management</a></div>
            <div>
                <a class="admin_dashboard" href="<?php $config['hostname'] ?>index.php?admin=dashboard">
                    <i class="fas fa-chart-line"></i> Dashboard
                </a>
            </div>
            <div>
                <a class="admin_logs" href="<?php $config['hostname'] ?>index.php?admin=logs"><i class="fas fa-history"></i> Logs</a>
            </div>
            <div><a class="admin_setting" href="<?php $config['hostname'] ?>index.php?admin=setting"><i class="fas fa-cog"></i> Setting</a></div>
            <a href="./account/logout.php">
                <div class="admin_sidebar_logout"> Logout</div>
            </a>
        </div>

        <!-- content -->
        <div class="icFaBars_toggle"><i class="fas fa-bars"></i></div>
        <div class="admin_content">
        
            <!-- navBar -->
            <div class="admin_navBar">
                <div class="navBar_showAcc">
                    <!-- <i class="fas fa-user-circle"></i> -->
                    <img class="avatar" src="./img/<?= $ava ?>" alt=""> 
                    Welcome <?= $_SESSION["hoTen"] ?>
                    <div class="admin_option_acc">
                        <a href="./index.php?admin=setting">
                            <div><i class="fas fa-address-card"></i> Profile</div>
                        </a>
                        <a href="./account/logout.php">
                            <div><i class="fas fa-sign-out-alt"></i> Logout</div>
                        </a>
                    </div>
                </div>
            </div>

            <!-- detail -->
            <div class="main_content">
                <?= $contents ?>

            </div>
        </div>
    </div>

    <?php include_once './connecting/script.php'; ?>
</body>

</html>