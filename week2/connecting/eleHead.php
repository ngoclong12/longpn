<?php
$config = include_once $_SERVER['DOCUMENT_ROOT'] . '/longpn/week2/config.php';
?>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Dashboard</title>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
<link rel="stylesheet" href="<?= $config['hostname'] ?>lib/style.css">
<link rel="stylesheet" href="<?= $config['hostname'] ?>lib/animate-v4/animate.css">
<link rel="stylesheet" href="<?= $config['hostname'] ?>lib/bootstrap-v4/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= $config['hostname'] ?>lib/dropify/css/dropify.min.css">
