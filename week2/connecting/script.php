<?php
$sql = "SELECT * FROM dashboard";
$query = mysqli_query($connect, $sql);

?>

<script src="<?= $config['hostname'] ?>lib/bootstrap-v4/js/proper.min.js"></script>
<script src="<?= $config['hostname'] ?>lib/bootstrap-v4/js/jquery.js"></script>
<script src="<?= $config['hostname'] ?>lib/jquery/jquery.js"></script>
<script src="<?= $config['hostname'] ?>lib/bootstrap-v4/js/boostrap.js"></script>
<script src="./lib/chart-js/Chart.min.js"></script>
<!-- <script src="./lib/jquery/jquery.js"></script> -->
<script src="<?= $config['hostname'] ?>lib/dropify/js/dropify.min.js"></script>

<script>
    $(document).ready(function() {
        $('.navBar_showAcc').hover(function() {
            $('.admin_option_acc').slideToggle();
        })
        $('.icFaBars_toggle').click(function() {
            $('.admin_sidebar').fadeIn();
        });
        if ($(window).width() <= 500) {
            $('.admin_content').click(function() {
                $('.admin_sidebar').fadeOut();
            })
        }
        $('.admin_<?= $_GET['admin'] ?>').addClass('focus_sideBar');
        $('.pageNumber<?php if (isset($_GET['page'])) {echo $_GET['page'];} ?>').addClass('active');
        $('.pageNumber<?php if (!isset($_GET['page'])) {echo "1";} ?>').addClass('active');

        // dropify
        $('.dropify').dropify({
            messages: {
                'replace': 'Chạm để thay đổi',
                'remove': 'Gỡ bỏ',
                'error': 'Ooops !!!'
            }
        });

        $('.dropify').change(function() {
            let img = $(this).prop('files')[0];
            let nameImg = img.name;
            // console.log(nameImg);
            $.ajax({
                data: {
                    getImg: nameImg
                },
                url: './component/processAva.php',
                method: 'get',
            }).done(res => {
                console.log(res);
            }).fail(err => {
                console.log(err.response);
            })
        })

        $('.fix_profile').hover(function(){
            $('.alert_fix_profile').fadeToggle();
        })

        $("#exampleFormControlSelect1").change(function(){
            let select = $(this).find(":selected").val();
            console.log(select);
            $.ajax({
                data: {
                    getSelect: select
                },
                url: './component/processAva.php',
                method: 'get',
            }).done(res => {
                console.log(res);
                location.reload();
            }).fail(err => {
                console.log(err.response);
            })
        })

    })

    function prd_cart_del() {
        let conf = confirm('Bạn có chắc muốn xoá ?');
        return conf;
    }

    // pie chart data
    var pieData = [
        <?php
        while ($row = mysqli_fetch_array($query)) {
        ?> {
                value: 40,
                color: "<?= $row['color'] ?>"
            },
        <?php
        }
        ?>
    ];

    // pie chart options
    var pieOptions = {
        segmentShowStroke: false,
        animateScale: true
    }

    // get pie chart canvas
    var countries = document.getElementById("countries").getContext("2d");

    // draw pie chart
    new Chart(countries).Pie(pieData, pieOptions);
</script>