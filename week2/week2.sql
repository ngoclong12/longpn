-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 25, 2021 lúc 08:28 AM
-- Phiên bản máy phục vụ: 10.4.20-MariaDB
-- Phiên bản PHP: 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `week2`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dashboard`
--

CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `ip` varchar(255) NOT NULL,
  `devices` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `log_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `dashboard`
--

INSERT INTO `dashboard` (`id`, `name`, `ip`, `devices`, `color`, `log_id`) VALUES
(1, 'Linh', '255.255.0.1', '30', '#F9E4DC', 1),
(2, 'Long', '192.168.1.0', '50', '#428C9E', 2),
(17, 'Nam', '8.8.4.4', '50', '#41a765', 1),
(23, 'Tài', '1.1.1.1', '50', '#4b8d70', 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `list_admin`
--

CREATE TABLE `list_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `birth` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `list_admin`
--

INSERT INTO `list_admin` (`id`, `username`, `password`, `avatar`, `phone`, `birth`) VALUES
(1, 'john', '$2y$10$xbV4nCi1S7niZtUYb1eYCuMqidiGrBTFhhV6Dh5FBsHaYLXEgrYRm', 'kevin-bob-minions-1600x900.jpg', '0373295525', '2012-12-21'),
(2, 'ngoclong', '$2y$10$uAVClrYuP0s/ET/mxJnSyuhUaoGkZnKAh70Q9rkIzQM3oyQjVkh1y', 'Cho-–-ngu-–-ngay-–-nguyen-–-nhan-–-vi-–-dau-1.jpg', '0373295525', '2000-03-13'),
(5, 'thuylinh', '$2y$10$gTyFgpM4ihyvhyU0rgR46.FuhyxYx/AsmICINNt.ELg.2NRxYEdsq', 'default-avatar.png', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `logs`
--

CREATE TABLE `logs` (
  `device_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `logs`
--

INSERT INTO `logs` (`device_id`, `name`) VALUES
(1, 'Đào'),
(2, 'Mận'),
(4, 'Hồng'),
(5, 'Bưởi'),
(6, 'Lê'),
(7, 'Ổi'),
(8, 'Táo'),
(9, 'Mít'),
(10, 'Lựu');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `dashboard`
--
ALTER TABLE `dashboard`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ten` (`log_id`);

--
-- Chỉ mục cho bảng `list_admin`
--
ALTER TABLE `list_admin`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`device_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `dashboard`
--
ALTER TABLE `dashboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `list_admin`
--
ALTER TABLE `list_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `logs`
--
ALTER TABLE `logs`
  MODIFY `device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `dashboard`
--
ALTER TABLE `dashboard`
  ADD CONSTRAINT `fk_ten` FOREIGN KEY (`log_id`) REFERENCES `logs` (`device_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
