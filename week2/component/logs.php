<?php
ob_start();
include_once './component/ManageComponent.php';
include_once "./connecting/connectDB.php";

// add logs
$alert = "";
$b = new ManageComp();
if (isset($_POST["submitADD"])) {
    $name = $_POST["name_add"];
    $device_id = $_POST["id"];

    if (isset($name) && isset($device_id)) {
        // $sql = "INSERT INTO logs (name, device_id) VALUES ('$name', '$device_id')";
        // $query = mysqli_query($connect, $sql);
        // $alert = '<center class="alert alert-success animate__animated animate__slideOutUp">Thêm sản phẩm thành công !</center>';
        
        $alert = $b->logsAdd($name, $device_id, $connect);
        header("refresh: 2");
    } else {
        $alert = '<center class="alert alert-danger animate__animated animate__slideOutUp">Mời điền đủ thông tin sản phẩm !</center>';
    }
}

// Phân trang
$stt = 1;
if (isset($_GET["page"])) {
    $stt = $_GET["page"];
}
$pageRow = 5;
if (isset($_SESSION['getSelect'])) {
    $pageRow = $_SESSION['getSelect'];
}
// echo $pageRow;
$perRow = $stt * $pageRow - $pageRow;
$totalPage = ceil(mysqli_num_rows(mysqli_query($connect, "SELECT * FROM logs ")) / $pageRow);
$listPrd = "";
$x = 1;
$y = 2;

// Xử lý search
$textFix = "";
if (isset($_GET["search"])) {
    $text = $_GET["search"];
    $textFix = trim($text);
    $arr_textFix = explode(" ", $textFix);
    $textFix = implode("%", $arr_textFix);
    $textFix = "%" . $textFix . "%";
    $totalPage = ceil(mysqli_num_rows(mysqli_query($connect, "SELECT * FROM logs WHERE Name LIKE ('$textFix')")) / $pageRow);
}

for ($i = 1; $i <= $totalPage; $i++) {

    if (isset($_GET["page"])) {
        $x = $_GET["page"] - 1;
        $y = $_GET["page"] + 1;
    }

    if (isset($_GET["search"])) {
        $prevPg = '<li class="page-item"><a class="page-link" href="./index.php?admin=logs&search=' . $text . '&page=' . $x . '" aria-label="Previous"><span aria-hidden="true"><i class="fas fa-angle-left"></i></span></a></li>';
        $listPrd .= '<li class="page-item pageNumber' . $i . '"><a class="page-link" href="./index.php?admin=logs&search=' . $text . '&page=' . $i . '">' . $i . '</a></li>';
        $nextPg = '<li class="page-item"><a class="page-link" href="./index.php?admin=logs&search=' . $text . '&page=' . $y . '" aria-label="Next"><span aria-hidden="true"><i class="fas fa-angle-right"></i></span></a></li>';
    } else {
        $prevPg = '<li class="page-item"><a class="page-link" href="./index.php?admin=logs&page=' . $x . '" aria-label="Previous"><span aria-hidden="true"><i class="fas fa-angle-left"></i></span></a></li>';
        $listPrd .= '<li class="page-item pageNumber' . $i . '"><a class="page-link " href="./index.php?admin=logs&page=' . $i . '">' . $i . '</a></li>';
        $nextPg = '<li class="page-item"><a class="page-link" href="./index.php?admin=logs&page=' . $y . '" aria-label="Next"><span aria-hidden="true"><i class="fas fa-angle-right"></i></span></a></li>';
    }
}
// $sql1 = "SELECT * FROM logs ORDER BY device_id DESC LIMIT $perRow,$pageRow";
// $query1 = mysqli_query($connect, $sql1);
$query1 = $b->logsPaginateDefault($perRow, $pageRow, $connect);
if (isset($_GET["search"])) {
    // $sql2 = "SELECT * FROM logs WHERE Name LIKE ('$textFix') ORDER BY device_id ASC LIMIT $perRow,$pageRow";
    // $query1 = mysqli_query($connect, $sql2);
    $query1 = $b->logsPaginateSearch($perRow, $pageRow, $textFix, $connect);
}

?>

<!-- heading bar -->
<?= $alert ?>
<div class="logs_heading">
    <h2>Action Logs</h2>
    <div class="form-group">
        <label for="exampleFormControlSelect1">Show lines</label>
        <select class="form-control" id="exampleFormControlSelect1">
            <?php if ($pageRow = 3) {
            ?>
                <option selected value="3">3</option>
                <option value="5">5</option>
                <option value="7">7</option>
            <?php
            }
            ?>
            <?php if ($pageRow = 5) {
            ?>
                <option style="display:none" selected value="<?= $pageRow ?>"><?= $_SESSION['getSelect'] ?? 5 ?></option>
            <?php
            }
            ?>
        </select>
    </div>
    <div>
        <form action="" method="get">
            <input type="hidden" name="admin" value="logs">
            <input id="searchName" name="search" type="search" placeholder="name" required>
            <input id="logs_inpSearch" type="submit" value="Search">
        </form>
    </div>
</div>

<!-- table -->
<table class="dashboard_table">
    <thead>
        <tr>
            <th>Device ID #</th>
            <th>Name</th>
            <th>Action</th>
            <th>Date</th>
            <th>Edit</th>
        </tr>
    </thead>
    <tbody id="push">
        <?php
        while ($row1 = mysqli_fetch_array($query1)) {
        ?>
            <tr>
                <td><?= $row1['device_id'] ?></td>
                <td><?= $row1['name'] ?></td>
                <td>turn off</td>
                <td>25-11-2021</td>
                <td>
                    <span class="btn btn-success btn-sm delete_row_logs"><i class="fas fa-edit"></i></span>
                    <a onclick="return prd_cart_del()" class="btn btn-danger btn-sm" href="./component/processAva.php?id_row=<?= $row1['device_id'] ?>">
                        <i class="fas fa-trash"></i>
                    </a>
                    <!-- <span class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></span> -->
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>

<!-- pagination -->
<nav class="logs_pagination" aria-label="Page navigation example">
    <ul class="pagination">
        <?php
        if (isset($_GET['page']) && $_GET['page'] > 1) {
            echo $prevPg;
        }
        ?>
        <?= $listPrd ?>
        <?php
        if (isset($_GET['page']) && $_GET['page'] < $totalPage) {
            // if($_GET['page'] != 1){
            echo $nextPg;
            // }
        }
        if (isset($_SESSION['getSelect'])) {
            if (!isset($_GET['page']) && mysqli_num_rows($query1) >= $_SESSION['getSelect']) {
                echo $nextPg;
            }
        } else {
            if (!isset($_GET['page']) && mysqli_num_rows($query1) >= 5) {
                echo $nextPg;
            }
        }
        
        ?>

    </ul>
</nav>

<!-- add -->
<div class="dashboard_addDevices" style="margin: auto;">

    <form action="http://localhost/longpn/week2/index.php?admin=logs" method="post">
        <div><input name="name_add" type="text" placeholder="name"></div>
        <div><input name="id" type="text" placeholder="ID"></div>
        <div>
            <button class="btn_login" name="submitADD" type="submit">Add Logs</button>
        </div>
    </form>
</div>

<?php
$contents = ob_get_clean();

?>