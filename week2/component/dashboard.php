<?php
ob_start();
include_once './component/ManageComponent.php';
include_once "./connecting/connectDB.php";
$total = 0;
$result = "";
$count = 0;
$randomcolor = '#' . dechex(rand(0, 10000000));

if (isset($_POST["submit"])) {

    $name = $_POST["name"];
    $ip = $_POST["ip"];
    $logs_id = $_POST["logs_id"];

    if (isset($name) && isset($ip) && isset($logs_id)) {
        $a = new ManageComp();
        $result = $a->dashboard($name, $ip, $randomcolor, $logs_id, $connect);
        
    } else {
        $result = '<center class="alert alert-danger animate__animated animate__slideOutUp">Mời điền đủ thông tin sản phẩm !</center>';
    }
}

$sql = "SELECT * FROM dashboard";
$query = mysqli_query($connect, $sql);
$sql1 = "SELECT name,color FROM dashboard";
$query1 = mysqli_query($connect, $sql1);



?>

<!-- table -->
<table class="dashboard_table">
    <thead>
        <tr>
            <th>Devices</th>
            <th>MAC address</th>
            <th>IP</th>
            <th>Created Date</th>
            <th class="th_width_right">Power Consumption (kw/h)</th>
        </tr>
    </thead>
    <tbody id="demo">
        <?php
        while ($row = mysqli_fetch_array($query)) {
            $total += $row['devices'];
            $count++;
        ?>
            <tr>
                <td><?= $row['name'] ?></td>
                <td>09:D2:13:A<?= $row['id'] ?></td>
                <td><?= $row['ip'] ?></td>
                <td>12-21-2021</td>
                <td class="th_width_right"><?= $row['devices'] ?></td>
            </tr>
        <?php
        }
        ?>
    </tbody>
    <tfoot>
        <tr class="dashboard_table_total">
            <td>Total</td>
            <td></td>
            <td></td>
            <td></td>
            <td id="total"><?= $total ?></td>
        </tr>
    </tfoot>
</table>

<!-- dashboard -->
<div class="block_dashboard">
    <div class="dashboard_circle">
        <h4>Power Consumption</h4>
        <div id="coloChart">
        <?php
        while ($row1 = mysqli_fetch_assoc($query1)) {

        ?>
        <?= $row1['name'] ?><span style="background-color: <?= $row1['color'] ?>"></span>
        <?php
        }
        ?>
        </div>
        <canvas id="countries" width="300" height="300"></canvas>
    </div>
    <div class="dashboard_addDevices">
        <form action="" method="post">
            <div><input name="name" id="name_addDiv" type="text" placeholder="name" required></div>
            <div><input name="ip" id="ip" type="text" placeholder="IP" required></div>
            <div><input name="logs_id" type="text" placeholder="Foreign Key" required></div>
            Foreign Key theo ID của bảng Logs
            <div>
                <button class="btn_login" name="submit" type="submit">
                    ADD DEVICES
                </button>
            </div>
        </form>
    </div>
</div>

<?php
$contents = ob_get_clean();
?>