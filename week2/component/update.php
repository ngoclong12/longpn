<?php
ob_start();
include_once './component/ManageComponent.php';
include_once "./connecting/connectDB.php";
$ID = $_SESSION["id"];

$d = new ManageComp();
$row = $d->updateGetinfo($ID, $connect);
// $row = mysqli_fetch_array($query);
$alert = "";

if (isset($_POST["submit"])) {


    if (isset($_POST["accept"])) {
        $imgName = $_FILES['img_up']['name'];
        $imgPath = $_FILES['img_up']['tmp_name'];
        if(isset($_POST["phone"]) && isset($_POST["birth"])){
            $phone = $_POST['phone'];
            $birth = $_POST['birth'];
            if (($_FILES['img_up']['name']) == '') {
                $alert = '<center class="alert alert-danger animate__animated animate__slideOutUp">Chọn ảnh mới để cập nhật !</center>';
            } else {
                
                $_SESSION["avatar_update"] = $imgName;
                move_uploaded_file($imgPath, './img/' . $imgName);
                // $sql = "UPDATE list_admin SET avatar = '$imgName',phone = '$phone', birth = '$birth'  WHERE id = '$ID'";
                // $query = mysqli_query($connect, $sql);
                // $alert = '<center class="alert alert-success animate__animated animate__slideOutUp update-av">Cập nhật thành công !</center>';
                $alert = $d->updated($imgName, $phone, $birth, $ID, $connect);
                header("refresh: 1");
            }
        }else{
            $alert = '<center class="alert alert-success animate__animated animate__slideOutUp update-av">Vui lòng điền đủ thông tin !</center>';
        }
    } else {
        $alert = '<center class="alert alert-danger animate__animated animate__slideOutUp update-av">Mời xác nhận trước khi cập nhật !</center>';
    }
}
?>

<div class="container">
    <?= $alert ?>
    
    <div class="row">
        <div class="col-md-8">
            <h5>Change Avatar</h5>
            <form method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleInputEmail1">Avatar</label>
                    <input type="file" class="dropify" name="img_up" data-default-file="./img/<?= $row['avatar'] ?>" />
                </div>
                <div class="form-group">
                    <div><label for="exampleInputEmail1">Username</label></div>
                    <div class="btn btn-light"><?= $row['username'] ?></div>
                </div>
                <div class="form-group">
                    <div><label for="exampleInputEmail1">Phone</label></div>
                    <input class="form-control" name="phone" type="text" placeholder="<?= $row['phone'] ?>" required>
                </div>
                <div class="form-group">
                    <div><label for="exampleInputEmail1">Birthday</label></div>
                    <input class="form-control" name="birth" type="date" value="<?= $row['birth'] ?>" required>
                </div>
                <div class="form-group">
                    <div><label for="exampleInputPassword1">Password</label></div>
                    <div class="btn btn-light"><?= $row['password'] ?></div>

                </div>
                <div>
                    <input type="checkbox" name="accept"> I accept
                </div>
                <button type="submit" name="submit" class="btn btn-primary mt-2">Submit</button>
            </form>
        </div>
    </div>
</div>

<?php
$contents = ob_get_clean();
?>