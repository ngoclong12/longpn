<?php
ob_start();
include_once './component/ManageComponent.php';
include_once "./connecting/connectDB.php";
$ID = $_SESSION["id"];

$c = new ManageComp();
$row = $c->setting($ID, $connect);

?>

<div class="container pb-5">
    <div class="row">
        <div class="col-md-8 setting_bgr_left">
            <h5>Personal information 
                <a class="fix_profile" href="./index.php?admin=update"><i class="fas fa-edit"></i></a>
                <span class="alert_fix_profile">Fix profile</span>
            </h5>
            <div class="form-group row mt-5">
                <label for="staticEmail" class="col-sm-2">Avatar:</label>
                <div class="col-sm-10">
                    <img class="avatar" src="./img/<?= $row['avatar'] ?>" alt="Avatar">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2">Username:</label>
                <div class="col-sm-10">
                    <?= $row['username'] ?>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2">Phone:</label>
                <div class="col-sm-10">
                    <?= $row['phone'] ?>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2">Birthday:</label>
                <div class="col-sm-10">
                    <?= $row['birth'] ?>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-2">Password:</label>
                <div class="col-sm-10">
                    <?= $row['password'] ?>
                </div>
            </div>
        </div>
        <div class="col-md-4 setting_bgr_right">
            <h5>Full size Avatar</h5>
            <div>
                <img class="avatarFull mt-5" src="./img/<?= $row['avatar'] ?>" alt="Avatar">
            </div>
        </div>
    </div>
</div>

<?php
$contents = ob_get_clean();
?>