<?php
session_start();
include_once "../connecting/connectDB.php";
include_once "../account/ManageAccount.php";
$rowPass = "";

if (isset($_POST["submit"])) {

    if (isset($_POST["username"]) && isset($_POST["password"])) {
        $username = $_POST["username"];
        $password = $_POST["password"];
        
        $a = new ManageAcc();
        $rowPass = $a->Login($username,$connect,$password);

    } else {
        $rowPass = '<center class="btn btn-danger">MờI nhập đầy đủ thông tin</center>';
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once '../connecting/eleHead.php' ?>
</head>

<body>
    <div class="login">
        <div class="block_login">
            <div class="block_login_heading">SOIOT SYSTEM</div>
            <?= $rowPass ?>
            <form action="" method="post">
                <div><input id="username" type="text" name="username" placeholder="username" required></div>
                <div><input id="password" type="password" name="password" placeholder="password" required></div>
                <div>
                    <button class="btn_login" name="submit" type="submit">LOGIN</button>
                    <a href="./registry.php" > or create new account</a>
                </div>

            </form>
        </div>
    </div>

</body>

</html>