<?php
include_once "../connecting/connectDB.php";
include_once "../account/ManageAccount.php";

$result = "";
if (isset($_POST["submit"])) {

    if (isset($_POST["username"]) && isset($_POST["password"]) && isset($_POST["phone"])) {
        $username = $_POST["username"];
        $password = $_POST["password"];
        $sdt = $_POST["phone"];
        
        $c = new ManageAcc();
        $result = $c->registry($username,$password,$connect,$sdt);
    } else {
        $result = '<center class="btn btn-danger col-sm-12">Vui lòng nhập đủ thông tin !</center>';
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once '../connecting/eleHead.php' ?>
</head>

<body>
    <!-- login -->
    <div class="login">
        <div class="block_login">
            <div class="block_login_heading">REGISTRY</div>
            <form action="" method="post">
                <div><input id="username" type="text" name="username" placeholder="username" required></div>
                <div><input id="phone" type="text" name="phone" maxlength="10" placeholder="phone number" required>
                </div>
                <div><input id="password" type="password" name="password" placeholder="password" required></div>
                <div><input id="entPassword" type="password" name="entPassword" placeholder="enter password" required>
                </div>
                <div>
                    <button class="btn_login" name="submit" type="submit">REGISTRY</button>
                </div>

            </form>
        </div>

</body>

</html>