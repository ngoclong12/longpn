<?php
class ManageAcc
{

    public function Login($username, $connect, $password)
    {

        $getPass = "SELECT password,id,avatar FROM list_admin WHERE username = '$username'";
        $queryPass = mysqli_query($connect, $getPass);
        $rowPass = mysqli_fetch_assoc($queryPass);

        if (mysqli_num_rows($queryPass) > 0) {
            if (password_verify($password, $rowPass["password"])) {
                $_SESSION["hoTen"] = $username;
                $_SESSION["id"] = $rowPass["id"];
                $_SESSION["avatar"] = $rowPass["avatar"];
                header('location: ../index.php?admin=dashboard');
            } else {
                $alert = '<center class="btn btn-danger">Mật khẩu không chính xác !</center>';
                return $alert;
            }
        } else {
            $alert = '<center class="btn btn-danger">Tài khoản không chính xác !</center>';
            return $alert;
        }
    }

    public function logout()
    {
        session_start();
        session_destroy();
        header('location: ../account/login.php'); 
    }

    public function registry($username,$password,$connect,$sdt)
    {
        
        $getUser = "SELECT username FROM list_admin WHERE username = '$username'";

        if (mysqli_num_rows(mysqli_query($connect, $getUser)) > 0) {
            $alert = '<center class="btn btn-danger">Tài khoản đã tồn tại !</center>';
            return $alert;
        } else {
            $pw_hash = password_hash($password, PASSWORD_DEFAULT);
            $update = "INSERT INTO list_admin (username, password, avatar, phone) VALUES ('$username', '$pw_hash', 'default-avatar.png', $sdt)";
            mysqli_query($connect, $update);
            header('location: ../account/login.php');

        }
    }
}
