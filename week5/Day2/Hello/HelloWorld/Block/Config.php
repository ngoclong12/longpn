<?php
namespace Hello\HelloWorld\Block;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Hello\HelloWorld\Helper\Data;

class Config extends Template
{
    protected Data $helper;

    public function __construct(Template\Context $context, Data $helper)
    {
        parent::__construct($context);
        $this->helper = $helper;

    }

    public function getModuleStatus(){
        return $this->helper->getGeneralConfig('status');
    }

    public function getTitle(){
        return $this->helper->getGeneralConfig('page_title');
    }

    public function getDescription(){
        return $this->helper->getGeneralConfig('page_description');
    }

}
