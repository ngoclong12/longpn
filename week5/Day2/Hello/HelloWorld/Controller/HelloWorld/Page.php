<?php

namespace Hello\HelloWorld\Controller\HelloWorld;

use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;

class Page extends Action
{
    protected PageFactory $pageFactory;
    protected RequestInterface $request;

    public function __construct(Context $context, PageFactory $pageFactory, RequestInterface $request)
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->request = $request;
    }


    public function execute()
    {

            $name = $this->getRequest()->getParam('name', 'Bss Group');
            $dob = $this->getRequest()->getParam('dob', '21-12-2012');
            $address = $this->getRequest()->getParam('address','48-ToHuu');
            $a = [
                'name' => $name,
                'dob' => $dob,
                'addr' => $address
            ];
            $page = $this->pageFactory->create();
            $page->getLayout()->getBlock('hello_helloworld_page')->setName($name)->setDob($dob)->setAddress($address);
            return $page;

    }
}
