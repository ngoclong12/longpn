<?php
namespace Hello\HelloWorld\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Page extends Template
{
    protected RequestInterface $request;

    public function __construct(Template\Context $context, RequestInterface $request)
    {
        parent::__construct($context);
        $this->request = $request;
    }

    public function getName(){
        return $this->getRequest()->getParam('name', 'Bss Group');
    }
    
    public function getDob(){
        return $this->getRequest()->getParam('dob', '21-12-2012');
    }
    
    public function getAddress(){
        return $this->getRequest()->getParam('address','48-ToHuu');
    }
}
