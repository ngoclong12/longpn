<?php
namespace Hello\HelloWorld\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Template;

class Router extends Template
{
    protected RequestInterface $request;

    public function __construct(Template\Context $context, RequestInterface $request)
    {
        parent::__construct($context);
        $this->request = $request;
    }

    public function getName(){
        return $this->request->getParam('name');
    }
}
