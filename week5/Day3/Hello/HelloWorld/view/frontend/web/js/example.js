define([
    'jquery'
], function ($) {
    'use strict';

    /*Khoi tao*/
    let id = 1;
    let html = '<tr class="tr1">' +
        '<td>' + id + '</td>' +
        '<td>' + 'Louis Vuitton' + '</td>' +
        '<td>' + '20.000$' + '</td>' +
        '<td>' + 'T-shirt' + '</td>' +
        '<td>12-21-2021</td>' +
        '<td><button class="btn1">DELETE</button</td>' +
        '</tr>';
    document.getElementById('demo').innerHTML = html;
    $('.btn1').click(function(){
        $('.tr1').remove();
    });

    /*render*/
    function render(html,id){

        document.getElementById('demo').innerHTML = html;
        del(id);
    }

    /*add*/
    $("#addProduct").click(function () {
        id += 1;
        let name = $("#name").val();
        let price = $("#price").val();
        let description = $("#description").val();
        // let file = $("#file").val();

            html += '<tr class="tr' + id + '">' +
                '<td>' + id + '</td>' +
                '<td>' + name + '</td>' +
                '<td>' + price + '</td>' +
                '<td>' + description + '</td>' +
                '<td>12-21-2021</td>' +
                '<td><button class="btn'+ id +'">DELETE</button</td>' +
                '</tr>';

        render(html,id);

    });

    /*del*/
    function del(id){
        for (let x=1; x<=id; x++){
            $('.btn'+ x +'').click(function(){
                $('.tr'+ x +'').remove();
            });
        }

    }

});
