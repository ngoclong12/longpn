require([
    'jquery',
    'Magento_Ui/js/modal/prompt'
], function($, prompt) { // Variable that represents the `prompt` function

    const id = 1;
    let html = '<tr id="1">' +
        '<td>' + id + '</td>' +
        '<td>' + 'Louis Vuitton' + '</td>' +
        '<td>' + '20.000$' + '</td>' +
        '<td>' + 'T-shirt' + '</td>' +
        '<td>12-21-2021</td>' +
        '<td><button id="btn1">DELETE</button</td>' +
        '</tr>';

    function render(html,id){
        $('#btn'+ id +'').click(function(){
            prompt({
                title: $.mage.__('test'),
                content: $.mage.__('test'),
                buttons: [{
                    text: $.mage.__('Cancel'),
                    class: 'action-secondary action-dismiss',
                    click: function () {
                        this.closeModal();
                        return false;
                    }
                }, {
                    text: $.mage.__('OK'),
                    class: 'action-primary action-accept',
                    click: function () {
                        let id = $(fed).attr('id');
                        deleteProduct(id);
                        this.closeModal(true);
                    }
                }],
                actions: {
                    confirm: function(){},
                    cancel: function(){},
                    always: function(){}
                },
                closeModal: function (result) {
                    var value;

                    if (result) {
                        if (this.options.validation && !this.validate()) {
                            return false;
                        }

                        value = this.modal.find(this.options.promptField).val();
                        this.options.actions.confirm.call(this, value);
                    } else {
                        this.options.actions.cancel.call(this, result);
                    }

                    this.options.actions.always();
                    this.element.on('promptclosed', _.bind(this._remove, this));

                    return this._super();
                }
            });
            $('#'+ id +'').remove();
        });
        document.getElementById('demo').innerHTML = html;
    }

    $("#addProduct").click(function () {
        id += 1;
        let name = $("#name").val();
        let price = $("#price").val();
        let description = $("#description").val();
        // let file = $("#file").val();

        html += '<tr id="' + id + '">' +
            '<td>' + id + '</td>' +
            '<td>' + name + '</td>' +
            '<td>' + price + '</td>' +
            '<td>' + description + '</td>' +
            '<td>12-21-2021</td>' +
            '<td><button id="btn'+ id +'">DELETE</button</td>' +
            '</tr>';

        render(html,id);

    });


});
