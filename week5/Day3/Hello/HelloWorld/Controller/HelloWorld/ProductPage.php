<?php

namespace Hello\HelloWorld\Controller\HelloWorld;

use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;

class ProductPage extends Action
{
    protected PageFactory $pageFactory;
    protected RequestInterface $request;

    public function __construct(Context $context, PageFactory $pageFactory, RequestInterface $request)
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->request = $request;
    }


    public function execute()
    {
        return $this->pageFactory->create();
    }
}
