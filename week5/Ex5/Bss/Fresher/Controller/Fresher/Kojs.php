<?php
declare(strict_types=1);

namespace Bss\Fresher\Controller\Fresher;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 */
class Kojs implements HttpGetActionInterface
{

    /**
     * Provides metadata about an attribute.
     *
     * @var PageFactory
     */
    protected PageFactory $pageFactory;

    // @codingStandardsIgnoreStart

    public function __construct(PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
    }

    public function execute()
    {
        return $this->pageFactory->create();
    }
}
