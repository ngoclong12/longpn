define(['jquery', 'uiComponent', 'ko'], function ($, Component, ko) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Bss_Fresher/hello'
            },
            
            initialize: function () {
                let self = this;
                this.data = ko.observableArray([]);
                this.cusId = ko.observable('');
                this.cusName = ko.observable('');
                this.cusClass = ko.observable('');
                this.validateName = ko.observable('');
                this.validateId = ko.observable('');
                this.validateClass = ko.observable('');
                this.delete = function (data) {
                    self.data.remove(this);
                }
                this._super();
            },

            submit : function () {
                
                let x = 0;
                let y = 0;
                let z = 0;
                if (this.cusName() == '') {
                    this.validateName("Moi nhap ten!");
                } else {
                    x = 1;
                    this.validateName("");
                }
                if (this.cusId() == '') {
                    this.validateId("Moi nhap id!");
                } else {
                    y = 1;
                    this.validateId("");
                }
                if (this.cusClass() == '') {
                    this.validateClass("Moi nhap class!");
                } else {
                    z = 1;
                    this.validateClass("");
                }
                if (x == 1 && y==1 && z==1) {
                    this.data.unshift({
                            cusId: this.cusId(),
                            cusName: this.cusName(),
                            cusClass: this.cusClass(),
                        }
                    );
                    this.cusId('');
                    this.cusName('');
                    this.cusClass('');
                }
            },
            
            reset : function () {
                this.data([]);
            },
            
        });
    }
);
