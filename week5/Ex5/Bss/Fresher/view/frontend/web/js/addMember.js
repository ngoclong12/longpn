define([
    'jquery',
    'jquery/ui',
], function ($) {
    'use strict';

    $.widget('add.member', {
        options: {
            add: '#submit',
            check: 0,
            validateName: '#validateName',
            validateTelephone: '#validateTelephone',
            validateDob: '#validateDob',
        },

        _create: function () {
            let self = this;
            $(this.options.add).click(function () {
                self._addProduct();
            });
            this._reValidate();

        },

        _addProduct: function (check) {
            let self = this;

            /*get information*/
            let name = document.getElementById("name").value;
            let telephone = document.getElementById("telephone").value;
            let dob = document.getElementById("dob").value;
            let message = document.getElementById("message").value;

            /*Validate required*/
            let x = 0;
            let y = 0;
            let z = 0;
            if (name == '' && x == 0) {
                $('#validateName').show();
            } else {
                x = 1;
            }
            if (telephone == '' && y == 0) {
                $('#validateTelephone').show();
            } else {
                y = 1;
            }
            if (dob == '' && z == 0) {
                $('#validateDob').show();
            } else {
                z = 1;
            }
            if (x == 1 && y == 1 && z == 1) {
                self.options.check = 1;

                /*render info*/
                document.getElementById('popupName').innerHTML = "Name: " + name;
                document.getElementById('popupTele').innerHTML = "Phone: " + telephone;
                document.getElementById('popupDob').innerHTML = "Date of birth: " + dob;
                document.getElementById('popupMess').innerHTML = "Message: " + message;
            }

            /*modal*/
            require(["jquery", "Magento_Ui/js/modal/modal"], function ($, modal) {
                let options = {
                    type: 'popup',
                    responsive: true,
                    title: 'Your information',
                    buttons: ''
                };
                let popup = modal(options, $('#modal-content'));
                if (self.options.check == 1) {
                    $('#modal-content').modal('openModal');
                }
            });

        },

        _reValidate: function () {
            let self = this;

            $('#name').change(function () {
                $(self.options.validateName).hide();
                if ($(this).val() == '') {
                    $(self.options.validateName).show();
                    self.options.check = 0
                }
            })
            $('#telephone').change(function () {
                $(self.options.validateTelephone).hide();
                if ($(this).val() == '') {
                    $(self.options.validateTelephone).show();
                    self.options.check = 0;
                }
            })
            $('#dob').change(function () {
                $(self.options.validateDob).hide();
                if ($(this).val() == '') {
                    $(self.options.validateDob).show();
                    self.options.check = 0;
                }

            })

        }

    });

    return $.add.member;
});
