<?php
namespace Hello\Helloworld\Controller\HelloWorld;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Hello\Helloworld\Helper\Data;

class index extends Action
{

    protected $helperData;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Hello\HelloWorld\Helper\Data $helperData

    )
    {
        $this->helperData = $helperData;
        return parent::__construct($context);
    }

    public function execute()
    {

        // TODO: Implement execute() method.

        echo $this->helperData->getGeneralConfig('enable');
        echo $this->helperData->getGeneralConfig('page_title');
    }
}
