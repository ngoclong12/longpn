<?php

namespace Hello\Helloworld\Controller\HelloWorld;

use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;

class returnjson extends Action
{
    protected $pageFactory;
    protected $request;
    protected $json;

    public function __construct(Context $context, PageFactory $pageFactory, RequestInterface $request)
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->request = $request;
    }


    public function execute()
    {
        $name = $this->getRequest()->getParam('name', 'Bss Group');
        $dob = $this->getRequest()->getParam('dob', '21-12-2012');
        $addr = $this->getRequest()->getParam('address','48-ToHuu');
        $a = [
            'name' => $name,
            'dob' => $dob,
            'addr' => $addr
        ];
        $this->json = json_encode($a);
        $page = $this->pageFactory->create();
        $page->getLayout()->getBlock('hello_helloworld_returnjson')->setJson($this->json);
        return $page;
    }
}
