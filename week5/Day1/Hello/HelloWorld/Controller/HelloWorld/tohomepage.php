<?php
declare(strict_types=1);

namespace Hello\HelloWorld\Controller\HelloWorld;

use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

/**
 * Class Index
 */
class tohomepage extends Action
{
    private $forwardFactory;
    public function __construct(Context $context, ForwardFactory $forwardFactory)
    {
        parent::__construct($context);
        $this->forwardFactory = $forwardFactory;
    }


    public function execute()
    {
        $resultForward = $this->forwardFactory->create();
        $resultForward->setModule('cms')
            ->setController('index')
            ->forward('index');
        return $resultForward;
    }
}