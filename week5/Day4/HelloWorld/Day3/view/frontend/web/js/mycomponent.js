define(['jquery', 'uiComponent', 'ko'], function ($, Component, ko) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'HelloWorld_Day3/hello'
            },
            initialize: function () {
                this.data = ko.observableArray([]);
                this.productName = ko.observable('');
                this.productPrice = ko.observable('');
                this.productDescription = ko.observable('');
                this.validateName = ko.observable('');
                this.validatePrice = ko.observable('');
                this.validateDescription = ko.observable('');
                this._super();
            },

            addProduct : function (){
                
                let x = 0;
                let y = 0;
                let z = 0;
                if (this.productName() == ''){
                    this.validateName("Moi nhap ten san pham!");
                }else {
                    x = 1;
                    this.validateName("");
                }
                if (this.productPrice() == ''){
                    this.validatePrice("Moi nhap gia san pham!");
                }else {
                    y = 1;
                    this.validatePrice("");
                }
                if (this.productDescription() == ''){
                    this.validateDescription("Moi nhap chi tiet san pham!");
                }else {
                    z = 1;
                    this.validateDescription("");
                }
                if (x == 1 && y==1 && z==1){
                    this.data.unshift({
                            productName: this.productName(),
                            productPrice: this.productPrice(),
                            productDescription: this.productDescription(),
                        }
                    );
                }
            }
        });
    }
);
