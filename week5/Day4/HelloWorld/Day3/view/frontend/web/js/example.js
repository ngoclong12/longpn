define([
    'jquery'
], function($){
    'use strict';

    const data = [];
    
    function printData(data) {
        let list = '';
        data.forEach(item => {
            list += `<tr>
        <td>${item.productName}</td>
        <td>${item.productPrice}</td>
        <td><img src="${item.productImage}" height="100" width="150"></td>
        <td>${item.productDescription}</td>
        <td><button type="submit" class="delete-product" id="${item.id}">del</button></td>
        </tr>`;
        })
        document.querySelector('tbody').innerHTML = list;

        $(".delete-product").click(function (){
            let id = $(this).attr('id');
            deleteProduct(id);
        })

        require([
            'jquery',
            'Magento_Ui/js/modal/prompt'
        ], function($, prompt) { // Variable that represents the `prompt` function

            prompt({
                title: $.mage.__('Some title'),
                content: $.mage.__('Some content'),
                actions: {
                    confirm: function(){},
                    cancel: function(){},
                    always: function(){}
                }
            });

        });
    }
    
    $("#addProduct").click(function (){
        let productName = document.getElementById("productName").value;
        let productPrice = document.getElementById("productPrice").value;
        let productDescription = document.getElementById("productDescription").value;
        let productImage = document.getElementById("productImage").value;
        let id = data.length + 1;
        data.push({"id": id, "productName" : productName, "productPrice": productPrice,"productImage":productImage, "productDescription":productDescription});
        printData(data);
    })
    
    function deleteProduct(id){
        for (let i = 0; i < data.length; i++) {
            if (data[i]['id'] == id) {
                data.splice(i, 1);
            }
        }
        printData(data);
    }
});
