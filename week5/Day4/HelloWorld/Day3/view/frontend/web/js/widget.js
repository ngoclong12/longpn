define([
    'jquery',
    'jquery/ui',
    'Magento_Ui/js/modal/prompt'
], function ($, prompt, _) {
    'use strict';
    
    $.widget('mage.mywidget', {
        option: {
            data : []
        },
        _create: function () {
            let data = [{
                'id': '1',
                'productName': 'Gucci',
                'productPrice': '50',
                'productImage': 'https://www.w3schools.com/images/lamp.jpg',
                'productDescription': 'T-shirt',
            }];
            this._printData(this.option.data);
            this._addProduct(this.option.data);
        },

        _printData: function (data) {
            let self = this;
            let list = '';
            data.forEach(item => {
                list += `<tr>
                <td>${item.productName}</td>
                <td>${item.productPrice}</td>
                <td><img alt="" src="${item.productImage}" height="20" width="30"></td>
                <td>${item.productDescription}</td>
                <td><button type="submit" class="delete-product" id="${item.id}">del</button></td>
                </tr>`;
            })
            document.querySelector('tbody').innerHTML = list;

            $(".delete-product").click(function () {
                console.log(1);
                let id = $(this).attr('id');
                require([
                    'jquery',
                    'Magento_Ui/js/modal/prompt'
                ], function ($, prompt) {
                    prompt({
                        title: $.mage.__('Bạn có chắc chắn muốn xóa ?'),
                        content: $.mage.__('test'),
                        promptContentTmpl: "",
                        actions: {
                            confirm: function () {
                                self._deleteProduct(id, data);

                            },
                            cancel: function () {
                            },
                            always: function () {
                            }
                        },
                    });

                });
            })
        },

        _addProduct: function (data) {
            let self = this;
            $("#addProduct").click(function () {
                let productName = document.getElementById("productName").value;
                let productPrice = document.getElementById("productPrice").value;
                let productDescription = document.getElementById("productDescription").value;
                let productImage = document.getElementById("productImage").value;
                let id = data.length + 1;
                data.push({
                    "id": id,
                    "productName": productName,
                    "productPrice": productPrice,
                    "productImage": productImage,
                    "productDescription": productDescription
                });
                self._printData(data);
            });
        },

        _deleteProduct: function (id,data) {
            let self = this;
            // console.log(0);
            for (let i = 0; i < data.length; i++) {
                if (data[i]['id'] == id) {
                    data.splice(i, 1);
                }
            }
            self._printData(data);
        },
    });

    return $.mage.mywidget;
});
