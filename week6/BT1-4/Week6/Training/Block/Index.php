<?php
/**
 * Test
 *
 * @copyright Copyright © 2021 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Week6\Training\Block;

use Magento\Framework\View\Element\Template;
use Week6\Training\Model\InfoCustomerFactory;
use Magento\Framework\App\RequestInterface;

class Index extends Template
{
    /**
     * Provides metadata about an attribute.
     *
     * @var InfoCustomerFactory
     */
    protected InfoCustomerFactory $infoCustomer;
    
    /**
     * Provides metadata about an attribute.
     *
     * @var RequestInterface
     */
    protected RequestInterface $request;

    public function __construct(Template\Context $context, InfoCustomerFactory $infoCustomer, RequestInterface $request)
    {
        parent::__construct($context);
        $this->infoCustomer = $infoCustomer;
        $this->request = $request;
    }
}
