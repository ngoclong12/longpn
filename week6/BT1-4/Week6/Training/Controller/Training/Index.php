<?php
/**
 * Test
 *
 * @copyright Copyright © 2021 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Week6\Training\Controller\Training;

use Magento\Framework\App\Action\Action;
use Week6\Training\Model\InfoCustomerFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;

class Index extends Action
{
    /**
     * Provides metadata about an attribute.
     *
     * @var InfoCustomerFactory
     */
    protected InfoCustomerFactory $infoCustomer;

    /**
     * Provides metadata about an attribute.
     *
     * @var PageFactory
     */
    protected PageFactory $pageFactory;

    /**
     * Provides metadata about an attribute.
     *
     * @var ResultFactory
     */
    protected ResultFactory $resultRedirect;

    /**
     * Provides metadata about an attribute.
     *
     * @var RequestInterface
     */
    protected RequestInterface $request;

    public function __construct(Context $context, PageFactory $pageFactory, RequestInterface $request, InfoCustomerFactory $infoCustomer, ResultFactory $result)
    {
        parent::__construct($context);
        $this->infoCustomer = $infoCustomer;
        $this->resultRedirect = $result;
        $this->pageFactory = $pageFactory;
        $this->request = $request;
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        $name = $this->request->getParam('customerName');
        $dob = $this->request->getParam('customerDob');
        $sex = $this->request->getParam('customerSex');
        $address = $this->request->getParam('customerAddress');
        $email = $this->request->getParam('customerEmail');
        $submit = $this->request->getParam('submit');

        if ($submit == 1) {

            if ($name != '' && $dob != '' && $sex != '' && $address != '' && $email != '') {

                $model = $this->infoCustomer->create();
                $model->addData([
                    "customer_name" => $name,
                    "customer_dob" => $dob,
                    "customer_sex" => $sex,
                    "customer_address" => $address,
                    "customer_email" => $email,
                ]);
                $saveData = $model->save();
                $this->messageManager->addSuccessMessage(__('Insert Record Successfully !'));
            } else {

                $this->messageManager->addErrorMessage(__('There was an error while saving Internship data, please try again!'));
            }

            return $resultRedirect;
        }

        return $this->pageFactory->create();
    }
}
