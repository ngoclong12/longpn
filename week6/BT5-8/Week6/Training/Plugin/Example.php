<?php

namespace Week6\Training\Plugin;

use Week6\Training\Model\InfoCustomerFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Customer\Model\Session;

class Example{

    protected ManagerInterface $messageManager;

    protected Session $customerSession;

    /**
     * Provides metadata about an attribute.
     *
     * @var InfoCustomerFactory
     */
    protected InfoCustomerFactory $infoCustomer;

    public function __construct(ManagerInterface $messageManager, Session $customerSession, InfoCustomerFactory $infoCustomer){
        $this->messageManager = $messageManager;
        $this->customerSession= $customerSession;
        $this->infoCustomer = $infoCustomer;
    }

    public function beforeExecute()
    {
        $emailCustomer = $this->customerSession->getCustomer()->getEmail();
        $check = $this->infoCustomer->create()->load($emailCustomer, 'customer_email')->getData();
        if (count($check) == 6){
            return $this->messageManager->addSuccessMessage('You got 50% off for all orders');
        }
        return '';
    }

}
