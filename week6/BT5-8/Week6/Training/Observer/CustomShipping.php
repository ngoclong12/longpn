<?php
namespace Week6\Training\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomShipping implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();
        if (empty($order) || empty($quote)) {
            return $this;
        }

        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress->getDeliveryDate()) {
            $order->setDeliveryDate(
                $shippingAddress->getDeliveryDate()
            )->save();
        }

        return $this;
    }
}
