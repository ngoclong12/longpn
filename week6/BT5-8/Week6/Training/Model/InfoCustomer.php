<?php
/**
 * Internship
 *
 * @copyright Copyright © 2021 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Week6\Training\Model;

use Magento\Framework\Model\AbstractModel;

class InfoCustomer extends AbstractModel
{
    public function _construct()
    {
        $this->_init("Week6\Training\Model\ResourceModel\InfoCustomer");
    }
}
