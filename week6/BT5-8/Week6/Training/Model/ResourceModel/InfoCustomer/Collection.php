<?php
/**
 * Internship
 *
 * @copyright Copyright © 2021 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Week6\Training\Model\ResourceModel\InfoCustomer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init("Week6\Training\Model\InfoCustomer", "Week6\Training\Model\ResourceModel\InfoCustomer");
    }
}
