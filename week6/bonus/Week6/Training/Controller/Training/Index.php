<?php
/**
 * Test
 *
 * @copyright Copyright © 2021 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Week6\Training\Controller\Training;

use Magento\Framework\App\Action\Action;
use Week6\Training\Model\InfoCustomerFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Week6\Training\Helper\Data;

class Index extends Action
{
    /**
     * Provides metadata about an attribute.
     *
     * @var InfoCustomerFactory
     */
    protected InfoCustomerFactory $infoCustomer;

    /**
     * Provides metadata about an attribute.
     *
     * @var PageFactory
     */
    protected PageFactory $pageFactory;

    /**
     * Provides metadata about an attribute.
     *
     * @var ResultFactory
     */
    protected ResultFactory $resultRedirect;

    /**
     * Provides metadata about an attribute.
     *
     * @var RequestInterface
     */
    protected RequestInterface $request;

    protected Data $helper;

    public function __construct(Context $context, PageFactory $pageFactory, RequestInterface $request, InfoCustomerFactory $infoCustomer, ResultFactory $result ,Data $helper)
    {
        parent::__construct($context);
        $this->infoCustomer = $infoCustomer;
        $this->resultRedirect = $result;
        $this->pageFactory = $pageFactory;
        $this->request = $request;
        $this->helper = $helper;
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        $stt = $this->helper->checkStatus();
        if ($stt == 0) {

            $this->messageManager->addErrorMessage(__('You do not have enough permissions to access this page, please contact the administrator!'));
            return $resultRedirect;
        }
        return $this->pageFactory->create();
    }
}
