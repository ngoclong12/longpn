<?php
/**
 * Test
 *
 * @copyright Copyright © 2021 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Week6\Training\Controller\Training;

use Magento\Framework\App\Action\Action;
use Week6\Training\Model\InfoCustomerFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Week6\Training\Helper\Data;

class SaveData extends Action
{
    /**
     * Provides metadata about an attribute.
     *
     * @var InfoCustomerFactory
     */
    protected InfoCustomerFactory $infoCustomer;

    /**
     * Provides metadata about an attribute.
     *
     * @var PageFactory
     */
    protected PageFactory $pageFactory;

    /**
     * Provides metadata about an attribute.
     *
     * @var ResultFactory
     */
    protected ResultFactory $resultRedirect;

    /**
     * Provides metadata about an attribute.
     *
     * @var RequestInterface
     */
    protected RequestInterface $request;

    /**
     * Provides metadata about an attribute.
     *
     * @var Data
     */
    protected Data $helper;

    public function __construct(Context $context, PageFactory $pageFactory, RequestInterface $request, InfoCustomerFactory $infoCustomer, ResultFactory $result, Data $helper)
    {
        parent::__construct($context);
        $this->infoCustomer = $infoCustomer;
        $this->resultRedirect = $result;
        $this->pageFactory = $pageFactory;
        $this->request = $request;
        $this->helper = $helper;
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        $name = $this->request->getParam('customerName');
        $dob = $this->request->getParam('customerDob');
        $sex = $this->request->getParam('customerSex');
        $address = $this->request->getParam('customerAddress');
        $email = $this->request->getParam('customerEmail');

        if ($name != '' && $dob != '' && $sex != '' && $address != '' && $email != '') {

            if ($this->validateTypeDob($dob) == true &&
                $this->validateTypeSex($sex) == true &&
                $this->validateTypeEmail($email) == true
            ) {

                $checkUniEmail = $this->infoCustomer->create()->load($email, 'customer_email')->getData();
                if (empty($checkUniEmail)) {

                    $model = $this->infoCustomer->create();
                    $model->addData([
                        "customer_name" => $name,
                        "customer_dob" => $dob,
                        "customer_sex" => $sex,
                        "customer_address" => $address,
                        "customer_email" => $email,
                        
                    ]);
                    $model->save();
                    $this->messageManager->addSuccessMessage(__('Insert Record Successfully !'));
                } else {

                    $this->messageManager->addErrorMessage(__('Email already exists !'));
                }
            } else {

                $this->messageManager->addErrorMessage(__('Input data is not in the correct format !'));
            }
        } else {

            $this->messageManager->addErrorMessage(__('There was an error while saving Internship data, please try again!'));
        }

        return $resultRedirect;
    }

    /**
     * Validate email
     *
     * @return boolean
     * @param string $email
     */
    public function validateTypeEmail($email): bool
    {
        if (!empty($email)) {

            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {

                return true;
            }
        }
        return false;
    }

    /**
     * Validate sex
     *
     * @return boolean
     * @param string $sex
     */
    public function validateTypeSex($sex): bool
    {
        if (!empty($sex)) {

            if ($sex == 'Male' || $sex == 'Female') {

                return true;
            }
        }
        return false;
    }

    /**
     * Validate dob
     *
     * @return boolean
     * @param string $dob
     */
    public function validateTypeDob($dob): bool
    {
        if (!empty($dob)) {
            $checkDate = explode("-", $dob);
            if (count($checkDate) == 3) {
                if (checkdate((int)$checkDate[1], (int)$checkDate[2], (int)$checkDate[0])) {
                    return true;
                }
            }
        }
        return false;
    }
}
