<?php

namespace Week6\Training\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Template;
use Week6\Training\Model\InfoCustomerFactory;

class Bloglist extends Template
{
    /**
     * Provides metadata about an attribute.
     *
     * @var InfoCustomerFactory
     */
    protected InfoCustomerFactory $infoCustomer;

    /**
     * Provides metadata about an attribute.
     *
     * @var RequestInterface
     */
    protected RequestInterface $request;

    public function __construct(Template\Context $context, InfoCustomerFactory $infoCustomer, RequestInterface $request)
    {
        $this->infoCustomer = $infoCustomer;
        parent::__construct($context);
        $this->request = $request;

    }
    
    public function getCustomer(){
        $id = $this->request->getParam('id');
        return $this->infoCustomer->create()->load($id, 'customer_id')->getData();
    }

}
