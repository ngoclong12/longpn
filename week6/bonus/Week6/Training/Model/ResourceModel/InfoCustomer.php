<?php
/**
 * Internship
 *
 * @copyright Copyright © 2021 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Week6\Training\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class InfoCustomer extends AbstractDb
{
    public function _construct()
    {
        $this->_init("info_customer", "customer_id");
    }
}
