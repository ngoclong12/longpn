<?php

namespace Week6\Training\Observer;

use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Event\ObserverInterface;
use Week6\Training\Model\InfoCustomerFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface;

class UpdateCustomerMeta implements ObserverInterface
{

    protected $_customerRepositoryInterface;

    /**
     * Provides metadata about an attribute.
     *
     * @var InfoCustomerFactory
     */
    protected InfoCustomerFactory $infoCustomer;

    /**
     * Provides metadata about an attribute.
     *
     * @var ResultFactory
     */
    protected ResultFactory $resultRedirect;
    protected ManagerInterface $messageManager;
    private RedirectInterface $_redirect;

    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface, InfoCustomerFactory $infoCustomer, ResultFactory $result, ManagerInterface $messageManager, RedirectInterface $_redirect
    )
    {
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->infoCustomer = $infoCustomer;
        $this->resultRedirect = $result;
        $this->messageManager = $messageManager;
        $this->_redirect = $_redirect;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        $customer = $observer->getEvent()->getCustomer();
        $name = $customer->getfirstname() . $customer->getlastname();
        $model = $this->infoCustomer->create()->load($customer->getEmail(), 'customer_email');
        $check = $model->getData();
        if (count($check) == 6) {

            $model->setData("customer_name", $name);
            $model->save();
        }
        $this->messageManager->addSuccessMessage(__('Insert Record Successfully !'));
        return $resultRedirect;
    }
}
