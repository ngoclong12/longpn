<?php

namespace Week6\Training\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const PATH = 'test/internship/enable';

    public function checkStatus()
    {
        return $this->scopeConfig->getValue(self::PATH, ScopeInterface::SCOPE_STORE);
    }

}
