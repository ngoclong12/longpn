<?php
namespace Week6\Training\Ui\Component\Listing\Grid\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;
use Magento\Framework\Url;

/**
 * Class Action
 * @package ViMagento\HelloWorld\Ui\Component\Listing\Grid\Column
 */
class Action extends Column
{
    /**
     * @var UrlInterface
     */
    protected UrlInterface $urlBuilder;
    protected Url $urlFrontend;

    /**
     * Action constructor.
     * @param UrlInterface $urlBuilder
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        UrlInterface $urlBuilder,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Url $urlFrontend,
        array $components = [],
        array $data = []

    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->urlFrontend = $urlFrontend;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item[$this->getData('name')] = [
                    'view' => [
                        'href' => $this->urlFrontend->getUrl('week6/training/view', ['id' => $item['customer_id']]),
                        'label' => __('View')
                    ],
                    'edit' => [
                        'href' => $this->urlBuilder->getUrl('test/test/listing', ['id' => $item['customer_id']]),
                        'label' => __('Edit')
                    ],
                ];
            }
        }

        return $dataSource;
    }
}
