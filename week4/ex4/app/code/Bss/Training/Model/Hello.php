<?php
namespace Bss\Training\Model;
use Bss\Training\Api\HelloInterface;

class Hello implements HelloInterface
{
    public function getProduct($id) {
        $arrays = [
            ['id'=>1, 'name'=>'product1', 'class'=>'class1'],
            ['id'=>2, 'name'=>'product2', 'class'=>'class2'],
            ['id'=>3, 'name'=>'product3', 'class'=>'class3'],
            ['id'=>4, 'name'=>'product4', 'class'=>'class4'],
            ['id'=>5, 'name'=>'product5', 'class'=>'class5'],
        ];
//        for($i=0;$i<count($arrays);$i++){
//            $arrays[$i]['id']= $id;
//            return $arrays[$i];
//        }
        if(1 <= $id && $id <= 5){
            return $arrays[$id];
        }
        else{
            return 'No data';
        }
    }
}
