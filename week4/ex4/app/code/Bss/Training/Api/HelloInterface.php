<?php

namespace Bss\Training\Api;

interface HelloInterface
{
    /**
     * Returns greeting message to user
     *
     * @param string $name Users name.
     * @return string Greeting message with users name.
     * @api
     */
    public function getProduct($id);
}
